# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.4.4

- patch: Internal maintenance: bump bitbucket-pipe-release.

## 0.4.3

- patch: Internal maintenance: change pipe metadata according to new structure

## 0.4.2

- patch: Internal maintenance: Add gitignore secrets.

## 0.4.1

- patch: Internal maintenance: Upgrade dependency version bitbucket-pipes-toolkit.
- patch: Update the Readme with a new Atlassian Community link.

## 0.4.0

- minor: Added DELETE_FLAG variable; Made --delete-after optional.

## 0.3.2

- patch: Updated readme with advantages of rsync.

## 0.3.1

- patch: Internal maintenance

## 0.3.0

- minor: Allowed using array variables to pass EXTRA_ARGS

## 0.2.1

- patch: Update pipes bash toolkit version.

## 0.2.0

- minor: Added SSH_PORT variable for custom ssh port

## 0.1.0

- minor: Initial release

